'use strict';

module.exports = {
    env: 'development',
    port: process.env.PORT,
    aws: {
        region: 'us-east-1',
        bucket: ''
    },
    database: {
        uri: 'mongodb://apparelbox:apparelbox@localhost/apparelbox'
    },
    facebook: {
        clientId: '1903742196578849',
        clientSecret: 'e5cec784ca65903b55b113a295d8bcb6',
        callbackUrl: '/auth/facebook/callback',
        profileFields: ['id', 'emails', 'name']
    },
    kakaotalk: {
        clientId: '0dc03f7416d1abe5392e23fe36bf922a',
        callbackUrl: '/auth/kakao/callback'
    }
};
