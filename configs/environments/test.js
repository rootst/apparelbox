'use strict';

module.exports = {
    env: 'test',
    port: process.env.PORT,
    aws: {
        region: 'us-east-1',
        bucket: ''
    }
};
