'use strict';
const path = require('path');
const rootPath = path.normalize(__dirname + '/../..');


module.exports = {
    root: rootPath,
    sessionSecretKey: 'alskdWAE%J#$IJFKLerwioptjeopir',
    locales: ['ko_KR', 'en_US'],
    defaultLocale: 'en_US',
    pagination: {
        limit: 10
    },
    
    userRole: {
        administrators: ['ADMINISTRATOR'],
        clerks: ['STYLIST'],
        clients: ['CLIENT']
    },
    userInformationStatus: {
        temporary: 'TEMPORARY',
        completed: 'COMPLETED'
    },

    orderType: {
        normal: 'NORMAL',
        subscription: 'SUBSCRIPTION'
    },
    orderStatus: {
        receipted: 'RECEIPTED',
        styling: 'STYLING',
        delivering: 'DELIVERING',
        Redelivering: 'REDELIVERING',
        completed: 'COMPLETED'
    },
    
    apparelType: {
        top: {
            tShirt: 'T-SHIRT', // 티셔츠
            turtleNeck: 'TURTLE NECK', // 폴라티
            tankTop: 'TANK TOP', // 나시
            sweatShirt: 'SWEAT SHIRT', // 맨투맨
            dressShirt: 'DRESS SHIRT', // 와이셔츠
            piqueShirt: 'PIQUE SHIRT', // 카라티
            sweater: 'SWEATER', // 스웨터
            cardigan: 'CARDIGAN' // 가디건
        },
        bottoms: {
            jean: 'JEAN', // 청바지
            chinoPants: 'CHINO PANTS', // 치노
            slacks: 'SLACKS', // 슬랙스
            formal: 'FORMAL' // 정장
        },
        jacket: {
            down: 'DOWN', // 패딩
            parka: 'PARKA', // 파카
            military: 'MILITARY', // 야상
            blouson: 'BLOUSON', // 항공점퍼, 야구점퍼
            blazer: 'BLAZER', // 블레이져
            suit: 'SUIT' // 수트
        },
        coat: {
            chesterfield: 'CHESTERFIELD', // 체스터필드
            pea: 'PEA', // 피코트
            balmacaan: 'BALMACAAN', // 발마칸
            polo: 'POLO', // 폴로
            britishWarm: 'BRITISH WARM', // 브리티쉬 웜
            trench: 'TRENCH', // 트렌치
            duffle: 'DUFFLE' // 더플
        },
        accessory: {
            bracelet: 'BRACELET',
            watch: 'WATCH',
            bag: 'BAG',
            handBag: 'HANDBAG'
        },
        shoe: {
            oxford: 'OXFORD', // 옥스포드
            slipOn: 'SLIP-ON', // 슬립온
            sneakers: 'SNEAKERS' // 스니커즈
            /*
            highHeel: 'HIGH HEEL', // 하이힐
            boots: 'BOOTS', // 부츠
            bootee: 'BOOTEE', // 부티
            flat: 'FLAT', // 플랫
            sandal: 'SANDAL', // 샌달
            platformShoes: 'PLATFORM', // 플랫폼 슈즈(가보시)
            wedgeHeel: 'WEDGE HEEL', // 웨지힐
            sneakers: 'SNEAKERS', // 스니커즈
            slingBack: 'SLING BACK', // 슬링백
            openToe: 'OPEN TOE', // 오픈 토
            */
        }
    },

    gender: [
        {index: 1, content: 'MALE'},
        {index: 2, content: 'FEMALE'}
    ],
    dressStyle: [
        {index: 1, content: 'STYLE'},
        {index: 2, content: 'COMFORT'}
    ],
    officeDressCode: [
        {index: 1, content: 'NO_DRESS_CODE'},
        {index: 2, content: 'BUSINESS_CASUAL'},
        {index: 3, content: 'BUSINESS_FORMAL'},
        {index: 4, content: 'UNIFORM'}
    ],
    budget: [
        {index: 1, content: 'UNDER_50,000'},
        {index: 2, content: '50,000-75,000'},
        {index: 3, content: '75,000-100,000'},
        {index: 4, content: '100,000-125,000'},
        {index: 5, content: 'OVER_125,000'}
    ],
    selection: [
        {index: 1, content: 'EVERY_DAY'},
        {index: 2, content: 'RARELY_OR_NEVER'},
        {index: 3, content: '3-4_TIMES_PER_WEEK'},
        {index: 4, content: '1-2_TIMES_PER_WEEK'},
        {index: 5, content: '1-2_TIMES_PER_MONTH'}
    ],
    size: {
        shirt: [
            {index: 1, content: 'XS'},
            {index: 2, content: 'S'},
            {index: 3, content: 'M'},
            {index: 4, content: 'L'},
            {index: 5, content: 'XL'},
            {index: 6, content: 'XXL'}
        ],
        waist: [
            {index: 1, content: 28},
            {index: 2, content: 29},
            {index: 3, content: 30},
            {index: 4, content: 31},
            {index: 5, content: 32},
            {index: 6, content: 33},
            {index: 7, content: 34},
            {index: 8, content: 35},
            {index: 9, content: 36},
            {index: 10, content: 38},
            {index: 11, content: 40},
            {index: 12, content: 42}
        ],
        blazer: [
            {index: 1, content: 34},
            {index: 2, content: 36},
            {index: 3, content: 38},
            {index: 4, content: 40},
            {index: 5, content: 42},
            {index: 6, content: 44},
            {index: 7, content: 46},
            {index: 8, content: 48},
        ],
        shoe: [
            {index: 1, content: 240},
            {index: 2, content: 245},
            {index: 3, content: 250},
            {index: 4, content: 255},
            {index: 5, content: 260},
            {index: 6, content: 265},
            {index: 7, content: 270},
            {index: 8, content: 275}
        ]
    },
    fit: [
        {index: 1, content: 'SKINNY'},
        {index: 2, content: 'SLIM'},
        {index: 3, content: 'REGULAR'},
        {index: 4, content: 'STRAIGHT'},
        {index: 5, content: 'RELAXED'},
        {index: 6, content: 'AT_THE_KNEE'},
        {index: 7, content: 'ABOVE_KNEE'},
        {index: 8, content: 'LOWER_THIGH'},
        {index: 9, content: 'UPPER_THIGH'},
        {index: 10, content: 'TOO_SHORT'},
        {index: 11, content: 'TOO_TIGHT'},
        {index: 12, content: 'NO_ISSUE'},
        {index: 13, content: 'TOO_LOOSE'},
        {index: 14, content: 'TOO_LONG'}
    ],
    bodyShape: [
        {index: 1, content: 'SLIM'},
        {index: 2, content: 'AVERAGE'},
        {index: 3, content: 'ATHLETIC'},
        {index: 4, content: 'HUSKY'}
    ],
    dontCare: 'DO_NOT_CARE',
};