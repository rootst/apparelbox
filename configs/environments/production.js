'use strict';

module.exports = {
    env: 'production',
    port: process.env.PORT,
    aws: {
        region: 'ap-northeast-1',
        bucket: 'apparelbox'
    },
    database: {
        uri: 'mongodb://apparelbox:r1e2w3q4@54.65.5.75/apparelbox'
    },
    facebook: {
        clientId: '1903741853245550',
        clientSecret: '06f216ebbd613ef79b79058528f1debd',
        callbackUrl: '/auth/facebook/callback',
        profileFields: ['id', 'emails', 'name']
    },
    kakaotalk: {
        clientId: '0dc03f7416d1abe5392e23fe36bf922a',
        callbackUrl: '/auth/kakao/callback'
    }
};
