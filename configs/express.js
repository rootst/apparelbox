'use strict';
const path = require('path');
const express = require('express');
const morgan = require('morgan'); // express log
const bodyParser = require('body-parser'); // express json, express urlencoded
const cookieParser = require('cookie-parser'); // express cookieParser
const session = require('express-session'); // express session
const methodOverride = require('method-override'); // express method-override
const favicon = require('static-favicon'); // express favicon
const errorHandler = require('errorhandler'); // express errorHandler
const compression = require('compression');
const cors = require('cors');
const jade = require('jade');
const babel = require('jade-babel');

const config = require('./config');
const isDebug = require(config.root + '/utilities/isDebug');


module.exports = function(app) {
    if (isDebug()) {
        //app.use(require('connect-livereload')());

        // Disable caching of scripts for easier testing
        app.use((request, response, next) => {
            if (request.url.indexOf('/js/') === 0) {
                response.header('Cache-Control', 'no-cache, no-store, must-revalidate');
                response.header('Pragma', 'no-cache');
                response.header('Expires', 0);
            }
            next();
        });

        app.use(express.static(path.join(config.root, 'public')));
        app.set('views', config.root + '/public/views');

    } else {
        app.use(favicon(path.join(config.root, 'public/images', 'favicon.ico')));
        app.use(express.static(path.join(config.root, 'public')));
        app.use(express.static(path.join(config.root, 'node_modules')));
        app.set('views', config.root + '/public/views');
    }

    //app.locals.pretty = true;
    app.set('view engine', 'jade');
    jade.filters.babel = babel({});

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(compression());
    app.use(cookieParser());
    app.use(cors());
    app.use(morgan('dev'));
    app.use(methodOverride()); // simulate DELETE and PUT
    app.use(methodOverride('X-HTTP-Method')); // Microsoft
    app.use(methodOverride('X-HTTP-Method-Override')); // Google/GData
    app.use(methodOverride('X-Method-Override')); // IBM
    app.use(session({
        secret: config.sessionSecretKey,
        resave: false,
        saveUninitialized: true
    }));

    // Error handler
    if (isDebug()) {
        app.use(errorHandler());
    }
};
