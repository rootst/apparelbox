'use strict';
const _ = require('lodash');

let node_env = process.env.NODE_ENV;
if (_.isUndefined(node_env)) {
    node_env = 'test';
}

module.exports = _.merge(
    require('./environments/all.js'),
    require('./environments/' + node_env + '.js') || {}
);
