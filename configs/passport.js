'use strict';
const _ = require('lodash');
const mongoose = require('mongoose');
const FacebookStrategy = require('passport-facebook').Strategy;
const KakaoStrategy = require('passport-kakao').Strategy;
const LocalStrategy = require('passport-local').Strategy;
const flash = require('connect-flash');

const config = require('./config');
const User = require(config.root + '/models/user');


module.exports = function(app, passport) {
    let localOptions = {};
    localOptions.usernameField = 'email';
    localOptions.passwordField = 'password';
    localOptions.passReqToCallback = true;

    passport.use('local-login', new LocalStrategy(localOptions, (request, email, password, done) => {
        User.findOne({email: email}, (foundError, foundUser) => {
            if (foundError) {
                let errorMessage = foundError || {};
                errorMessage._status = 500;
                return done(errorMessage, false);
            }
            if (!foundUser) {
                let errorMessage = {};
                errorMessage._status = 404;
                return done(errorMessage, false);
            }
            if (_.isUndefined(foundUser.password)) {
                let errorMessage = {};
                errorMessage._status = 400;
                return done(errorMessage, false);
            }

            foundUser.comparePassword(password, (comparisonError, matchedResult) => {
                if (comparisonError) {
                    let errorMessage = comparisonError || {};
                    errorMessage._status = 500;
                    return done(errorMessage, false);
                }
                if (!matchedResult) {
                    let errorMessage = {};
                    errorMessage._status = 401;
                    return done(errorMessage, false);
                }

                request.login(foundUser._doc, (loggedInError) => {
                    if (loggedInError) {
                        let errorMessage = loggedInError || {};
                        errorMessage._status = 401;
                        return done(errorMessage, false);
                    }

                    foundUser.lastLoggedInDate = new Date();
                    foundUser.save((updateError, updatedUser) => {
                        if (updateError) {
                            let errorMessage = updateError || {};
                            errorMessage._status = 500;
                            return done(errorMessage, false);
                        }
                        if (!updatedUser) {
                            let errorMessage = {};
                            errorMessage._status = 500;
                            return done(errorMessage, false);
                        }

                        request.user = updatedUser;
                        done(null, updatedUser._doc);
                    });
                });
            });
        });
    }));

    let facebookOptions = {};
    facebookOptions.clientID = config.facebook.clientId;
    facebookOptions.clientSecret = config.facebook.clientSecret;
    facebookOptions.callbackURL = config.facebook.callbackUrl;
    facebookOptions.profileFields = config.facebook.profileFields;

    passport.use(new FacebookStrategy(facebookOptions, (accessToken, refreshToken, profile, done) => {
        // CELLPHONE authentication check
        User.findOne({'facebook.id' : profile.id}, (foundError, foundUser) => {
            if (foundError) {
                let errorMessage = foundError || {};
                errorMessage._status = 500;
                return done(errorMessage, false);
            }

            if (foundUser) {
                return done(null, foundUser);
            }

            if (_.isUndefined(profile.emails)) {
                // NOTE: THIS CASE IS CELLPHONE AUTHENTICATION
                let newUser = new User();
                newUser.facebook = {};
                newUser.facebook.id = profile.id;
                newUser.facebook.accessToken = accessToken;
                newUser.roles = _.concat(newUser.roles, config.userRole.clients);
                newUser.save((saveError, savedUser) => {
                    if (saveError) {
                        let errorMessage = saveError || {};
                        errorMessage._status = 500;
                        return done(errorMessage, false);
                    }
                    if (!savedUser) {
                        let errorMessage = {};
                        errorMessage._status = 500;
                        return done(errorMessage, false);
                    }

                    done(null, newUser);
                });

            } else {
                const email = profile.emails[0].value;
                    
                let newUser = new User();
                newUser.facebook = {};
                newUser.facebook.id = profile.id;
                newUser.facebook.accessToken = accessToken;
                newUser.email = email;
                newUser.displayName = profile.name.givenName + '_' + profile.name.familyName;
                newUser.roles = _.concat(newUser.roles, config.userRole.clients);
                newUser.save((saveError, savedUser) => {
                    if (saveError) {
                        let errorMessage = saveError || {};
                        errorMessage._status = 500;
                        return done(errorMessage, false);
                    }
                    if (!savedUser) {
                        let errorMessage = saveError || {};
                        errorMessage._status = 500;
                        return done(errorMessage, false);
                    }

                    done(null, newUser);
                });
            }
        });
    }));

    let kakaoOptions = {};
    kakaoOptions.clientID = config.kakaotalk.clientId;
    kakaoOptions.callbackURL = config.kakaotalk.callbackUrl;

    passport.use(new KakaoStrategy(kakaoOptions, (accessToken, refreshToken, profile, done) => {
        User.findOne({'kakao.id' : profile.id}, (foundError, foundUser) => {
            if (foundError) {
                let errorMessage = foundError || {};
                errorMessage._status = 500;
                return done(errorMessage, false);
            }

            if (foundUser) {
                return done(null, foundUser);
            }

            let newUser = new User();
            newUser.kakao = {};
            newUser.kakao.id = profile.id;
            newUser.displayName = profile.displayName;
            newUser.roles = _.concat(newUser.roles, config.userRole.clients);
            newUser.save((saveError, savedUser) => {
                if (saveError) {
                    let errorMessage = saveError || {};
                    errorMessage._status = 500;
                    return done(errorMessage, false);
                }
                if (!savedUser) {
                    let errorMessage = saveError || {};
                    errorMessage._status = 500;
                    return done(errorMessage, false);
                }

                done(null, newUser);
            });
        });
    }));

    passport.serializeUser((user, done) => {
        done(null, user._id);
    });
    
    passport.deserializeUser((id, done) => {
        User.findById(id, (foundError, foundUser) => {
            if (foundError) {
                let errorMessage = saveError || {};
                errorMessage._status = 500;
                return done(errorMessage, false);
            }
            if (!foundUser) {
                let errorMessage = saveError || {};
                errorMessage._status = 500;
                return done(errorMessage, false);
            }

            done(null, foundUser);
        });
    });
    
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(flash());
};
