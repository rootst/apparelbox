'use strict';
const mongoose = require('mongoose');

const config = require('./config');
const isDebug = require(config.root + '/utilities/isDebug');


module.exports = function(callback) {
    mongoose.Promise = Promise;
    mongoose.connect(config.database.uri);
    mongoose.set('debug', isDebug());

    let db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', callback);
};
