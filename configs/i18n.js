'use strict';
const i18n = require('i18n');

const config = require('./config');


module.exports = function(app) {
    i18n.configure({
        locales: config.locales,
        defaultLocale: config.defaultLocale,
        cookie: 'locale',
        directory: config.root + '/locales'
    });
    app.use(i18n.init);
};
