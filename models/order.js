'use strict';
const _ = require('lodash');
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');


let orderSchema = new mongoose.Schema({
    createdDate: { type: Date, default: Date.now },
    owner: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
    userInformation: { type: mongoose.Schema.Types.ObjectId, ref: 'UserInformation', required: true },
    type: { type: String, required: true },
    status: { type: String, required: true },

    address: { type: String, required: true }
});

orderSchema.plugin(mongoosePaginate);

if (_.indexOf(mongoose.modelNames(), 'Order') === -1) {
    module.exports = mongoose.model('Order', orderSchema);
} else {
    module.exports = mongoose.model('Order');
}