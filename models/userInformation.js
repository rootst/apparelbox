'use strict';
const _ = require('lodash');
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const config = require('../configs/config');


let userInformationSchema = new mongoose.Schema({
    createdDate: { type: Date, default: Date.now },
    owner: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
    status: { type: String, default: config.userInformationStatus.temporary },

    nickname: { type: String },
    gender: { type: String },
    dateOfBirth: { type: Date },//
    married: { type: Boolean },
    height: { type: Number },//
    weight: { type: Number },//
    bodyShape: { type: String },
    occupation: { type: String },//
    officeDressCode: { type: String },
    commutionWay: { type: String },//
    stylishOrComfort: { type: String },
    dryCleanPerMonth: { type: Boolean },

    size: {
        shirt: { type: String }, //
        waist: { type: Number },//
        inseam: { type: Number },//
        blazer: { type: String },//
        shoe: { type: Number }//
    },

    fit: {
        casualShirt: [{ type: String }],
        buttonUpShirt: [{ type: String }],
        jeans: [{ type: String }],
        pants: [{ type: String }],
        shortPants: [{ type: String }],
        hemPants: { type: Boolean },

        shirtCollar: { type: String, default: 'NO_ISSUE' },
        shirtShoulder: { type: String, default: 'NO_ISSUE' },
        sleeveLength: { type: String, default: 'NO_ISSUE' },
        pantLeg: { type: String, default: 'NO_ISSUE' },
        pantLength: { type: String, default: 'NO_ISSUE' }
    },

    frequency: {
        leisureWear: { type: String },
        everyDayCasual: { type: String },
        buisinessCasual: { type: String },
        buisinessFormal: { type: String },
        nightOutAttire: { type: String },
        workOutGear: { type: String },
        specialOccation: { type: String }
    },

    budget: {
        buttonUpShirt: { type: String },
        teesAndPolos: { type: String },
        sweaterAndSweatShirts: { type: String },
        pantsAndDenim: { type: String },
        shortPants: { type: String },
        blazersAndOuter: { type: String },
        shoes: { type: String }
    },

    color: {
        black: { type: String, default: 'DO_NOT_CARE' },
        blue: { type: String, default: 'DO_NOT_CARE' },
        brown: { type: String, default: 'DO_NOT_CARE' },
        green: { type: String, default: 'DO_NOT_CARE' },
        grey: { type: String, default: 'DO_NOT_CARE' },
        khaki: { type: String, default: 'DO_NOT_CARE' },
        lightBlue: { type: String, default: 'DO_NOT_CARE' },
        navy: { type: String, default: 'DO_NOT_CARE' },
        olive: { type: String, default: 'DO_NOT_CARE' },
        pink: { type: String, default: 'DO_NOT_CARE' },
        purple: { type: String, default: 'DO_NOT_CARE' },
        red: { type: String, default: 'DO_NOT_CARE' },
        salmon: { type: String, default: 'DO_NOT_CARE' },
        white: { type: String, default: 'DO_NOT_CARE' },
        yellow: { type: String, default: 'DO_NOT_CARE' }
    },
    pattern: {
        flower: { type: String },
        check: { type: String },
        stripe: { type: String },
        paisley: { type: String },
        dot: { type: String },
        leopard: { type: String },
        military: { type: String },
        animal: { type: String },
        ethnic: { type: String },
        Leteral: { type: String }
    },
    material: {
        leather: { type: String },
        lace: { type: String },
        knit: { type: String },
        tweed: { type: String },
        sequin: { type: String },
        velvet: { type: String },
        seeThrough: { type: String },
        silky: { type: String },
        fur: { type: String },
        denim: { type: String },
        corduroy: { type: String },
        angora: { type: String },
        swade: { type: String }
    },
    detail: {

    },
    brand: [{ type: String }]
});


userInformationSchema.plugin(mongoosePaginate);


if (_.indexOf(mongoose.modelNames(), 'UserInformation') === -1) {
    module.exports = mongoose.model('UserInformation', userInformationSchema);
} else {
    module.exports = mongoose.model('UserInformation');
}