'use strict';
const _ = require('lodash');
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');


let billingRecordSchema = new mongoose.Schema({
    createdDate: { type: Date, default: Date.now },
    owner: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
    usedCard: { type: mongoose.Schema.Types.ObjectId, ref: 'Card', required: true },

    price: { type: Number, required: true }
});

billingRecordSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('BillingRecord', billingRecordSchema);
