'use strict';
const _ = require('lodash');
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
//const i18n = require('mongoose-i18n');
const bcrypt = require('bcrypt');


let userSchema = new mongoose.Schema({
    joinedDate: { type: Date, default: Date.now },
    lastLoggedInDate: { type: Date, default: Date.now },

    roles: [{ type: String, required: true }],
    email: { type: String, unique: true },
    password: { type: String },
    displayName: { type: String, required: true },
    phone: {
        number: { type: Number },
        verified: { type: Boolean, default: false }
    },

    administrator: {

    },

    facebook: {
        id: { type: String },
        accessToken: { type: String }
    },
    kakao: {
        id: { type: String }
    }
});

userSchema.pre('save', function(next) {
    if (!_.isUndefined(this.password) && (this.isModified('password') || this.isNew)) {
        bcrypt.genSalt(10, (saltError, salt) => {
            if (saltError) {
                let errorMessage = saltError || {};
                errorMessage._status = 500;
                return next(errorMessage);
            }

            bcrypt.hash(this.password, salt, (hashError, hash) => {
                if (hashError) {
                    let errorMessage = hashError || {};
                    errorMessage._status = 500;
                    return next(errorMessage);
                }

                this.password = hash;
                next();
            });
        });

    } else {
        next();
    }
});

userSchema.methods.comparePassword = function(password, callback) {
    bcrypt.compare(password, this.password, (errorResult, isMatch) => {
        if (errorResult) {
            errorResult._status = 500;
            return callback(errorResult, null);
        }
        callback(null, isMatch);
    });
};


userSchema.plugin(mongoosePaginate);
//userSchema.plugin(i18n, {languages: config.locales, defaultLanguage: config.defaultLocale});

if (_.indexOf(mongoose.modelNames(), 'User') === -1) {
    module.exports = mongoose.model('User', userSchema);
} else {
    module.exports = mongoose.model('User');
}