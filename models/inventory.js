'use strict';
const _ = require('lodash');
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');


let inventorySchema = new mongoose.Schema({
    createdDate: { type: Date, default: Date.now },
    reStockedDate: { type: Date, default: Date.now },
    type: { type: String, required: true },
    name: { type: String, required: true },
    size: { type: String, required: true },
    amount: { type: Number, required: true },
    image: { type: String }
});

inventorySchema.plugin(mongoosePaginate);

if (_.indexOf(mongoose.modelNames(), 'Inventory') === -1) {
    module.exports = mongoose.model('Inventory', inventorySchema);
} else {
    module.exports = mongoose.model('Inventory');
}