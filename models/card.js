'use strict';
const _ = require('lodash');
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');


let cardSchema = new mongoose.Schema({
    createdDate: { type: Date, default: Date.now },
    owner: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
    selected: { type: Boolean, default: false },

    company: { type: String, required: true },
    number: { type: String, required: true },
    expirationDate: { type: Date, required: true },
    CVC: { type: Number, required: true }
});

cardSchema.plugin(mongoosePaginate);

if (_.indexOf(mongoose.modelNames(), 'Card') === -1) {
    module.exports = mongoose.model('Card', cardSchema);
} else {
    module.exports = mongoose.model('Card');
}