'use strict';
/* INITIALIZE BEGIN */

// set default node js environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
if (process.env.NODE_ENV === 'development') {
    process.env.PORT = process.env.PORT || 3000;
} else if (process.env.NODE_ENV === 'production') {
    process.env.PORT = process.env.PORT || 8081;
} else {
    console.error('NODE_ENV ERROR');
    return;
}

/* INITIALIZE END */

const express = require('express');

const config = require('./configs/config');
const app = express();


// Initialize DB
require(config.root + '/configs/database')(() => {
    // NOTE: DO NOT CHANGE THIS SEQUENCE.
    require(config.root + '/configs/express')(app);
    require(config.root + '/configs/passport')(app, require('passport'));
    require(config.root + '/configs/i18n')(app);

    // GLOBAL VARIABLE SETTINGS
    app.use((request, response, next) => {
        response.locals = {
            user: request.user,
            authenticated: request.isAuthenticated()
        };
        next();
    });

    // ROUTES SETTINGS
    app.use('/', require(config.root + '/routes/routes'));
    app.use('/api', require(config.root + '/routes/apiRoutes'));

    // Initialize other module
    require(config.root + '/utilities/userJudge');
    require(config.root + '/utilities/errorHandling')(app);
    //require(config.root + '/utilities/scheduleLoader')();

    app.listen(config.port, () => {
        console.log('Server is running...');
        console.log('====================');
    });
});

module.exports = app;
