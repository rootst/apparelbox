
var User = require('./../models/users/user.js');
var UserType = require('./../models/users/userType.js');
var Project = require('./../models/projects/project.js');
var PlanStatus = require('./../models/planStatus.js');
var Category = require('./../models/category.js');

var UserRole = require('./../models/userRole');
router.get('/ps', (request, response) => {
    var status = new ResumeStatus();
    status.type = 'Submitted';
    status.save();

    var status1 = new ResumeStatus();
    status1.type = 'Saved';
    status1.save();

    var status2 = new ResumeStatus();
    status2.type = 'Verified';
    status2.save();

    var status3 = new ResumeStatus();
    status3.type = 'Accepted';
    status3.save();

    var status4 = new ResumeStatus();
    status4.type = 'Choosed';
    status4.save();

    response.send('z');
});
router.get('/ps', (request, response) => {
    var status = new ProjectStatus();
    status.type = 'Submitted';
    status.save();

    var status1 = new ProjectStatus();
    status1.type = 'Saved';
    status1.save();

    var status2 = new ProjectStatus();
    status2.type = 'Verified';
    status2.save();

    var status3 = new ProjectStatus();
    status3.type = 'Completed';
    status3.save();

    response.send('z');
});
router.get('/mt', (request, response) => {
    var status = new MeetingType();
    status.type.en_US = 'Offline';
    status.type.ko_KR = '오프라인';
    status.save();

    var status1 = new MeetingType();
    status1.type.en_US = 'Online';
    status1.type.ko_KR = '온라인';
    status1.save();

    response.send('z');
});

router.get('/pp', (request, response) => {
    var status = new ProjectRequestPurpose();
    status.type.en_US = 'Request';
    status.type.ko_KR = '프로젝트 요청';
    status.save();

    var status1 = new ProjectRequestPurpose();
    status1.type.en_US = 'Quotation';
    status1.type.ko_KR = '견적 문의';
    status1.save();

    response.send('z');
});

router.get('/pp', (request, response) => {
    var status = new PlanStatus();
    status.content.en_US = 'Idea';
    status.content.ko_KR = '아이디어';
    status.save();

    var status1 = new PlanStatus();
    status1.content.en_US = 'Note';
    status1.content.ko_KR = '필기';
    status1.save();

    var status2 = new PlanStatus();
    status2.content.en_US = 'Document';
    status2.content.ko_KR = '문서';
    status2.save();
    response.send('z');
});

router.get('/pp', (request, response) => {
    var status = new PlanStatus();
    status.content = 'IDEA';
    status.save();

    var status1 = new PlanStatus();
    status1.content = 'NOTE';
    status1.save();

    var status2 = new PlanStatus();
    status2.content = 'DOCUMENT';
    status2.save();
    response.send('z');
});

router.get('/ut', (request, response) => {
    var ut = new UserType();
    ut.type.en = 'Team';
    ut.type.kr = '팀';
    ut.save();

    var ut1 = new UserType();
    ut1.type.en = 'Personal';
    ut1.type.kr = '개인';
    ut1.save();

    var ut2 = new UserType();
    ut2.type.en = 'Personal Business';
    ut2.type.kr = '개인 사업자';
    ut2.save();

    var ut3 = new UserType();
    ut3.type.en = 'Corporation';
    ut3.type.kr = '법인 사업자';
    ut3.save();
    response.end();
});

router.get('/ca', (request, response) => {
    var status = new Category();
    status.name.en_US = 'Development';
    status.name.ko_KR = '개발';
    status.save((err)=>{
        console.log(err);
    });
    var status2 = new Category();
    status2.name.en_US = 'Web';
    status2.name.ko_KR = '웹';
    status2.parent = status;
    status2.save();

    var status3 = new Category();
    status3.name.en_US = 'Application';
    status3.name.ko_KR = '애플리케이션';
    status3.parent = status;
    status3.save();

    var status4 = new Category();
    status4.name.en_US = 'Design';
    status4.name.ko_KR = '디자인';
    status4.save((err)=>{
        console.log(err);
    });
    var status5 = new Category();
    status5.name.en_US = 'Logo';
    status5.name.ko_KR = '로고';
    status5.parent = status4;
    status5.save();

    var status6 = new Category();
    status6.name.en_US = 'Application';
    status6.name.ko_KR = '애플리케이션';
    status6.parent = status4;
    status6.save();


    response.send('z');
});

router.get('/test', (request, response) => {
    var number = 0;
    var i = 0;

    UserType.findOne({}).populate('userType', {'type.en': 'Personal' }).then((usage) => {
        if (!usage) {
            return response.status(404).json({error: 'usage not found.'});
        }
        for (i = 0; i < 50; i++) {
            var user = new User();
            user.client = true;
            user.email.address = 'xkguq'+number+'@gmail.com';
            user.password = 'qwer1234';
            user.userType = usage;
            user.userName = '김우영'+number;
            user.displayName.ko_KR = '김우영'+number;
            user.displayName.en_US = 'Wooyoung Connor Kim'+number;
            user.save();
            number++;
        }
        for (i = 50; i < 100; i++) {
            var user = new User();
            user.client = false;
            user.email.address = 'xkguq'+number+'@gmail.com';
            user.password = 'qwer1234';
            user.userType = usage;
            user.userName = '김우영'+number;
            user.displayName.ko_KR = '김우영'+number;
            user.displayName.en_US = 'Wooyoung Connor Kim'+number;
            user.save();
            number++;
        }
    }).catch((err) => {
        if (err) {
            console.log(err);
            return response.status(500).json({error: 'userType something wrong.'});
        }
    });

    number = 0;
    /*
    Category.findOne({'name.en': 'Application'}).populate('parent', {'name.en': 'Web' }).then((category) => {
        if (!category) {
            return response.status(404).json({error: 'category not found.'});
        }

        PlanStatus.findOne({content: 'IDEA'}).then((planStatus) => {
            if (!planStatus) {
                return response.status(404).json({error: 'planStatus not found.'});
            }
            for (i = 0; i < 100; i++) {
                var project = new Project();
                var date = new Date();
                project.title.kr = '제목' + number;
                project.title.en = 'title' + number;
                project.category = category;
                project.expirationDate = 1;
                project.budget = 100;
                project.planStatus = planStatus;
                project.planContent.kr = '테스트 내용' + number;
                project.planContent.en = 'test content' + number;
                project.recruitingExpirationDate = date;
                project.projectBeginningDate = date;
                project.projectManagingExpierence = false;
                project.save();
                number++;
            }
            return response.send('zxcv');
        }).catch((err) => {
            if (err) {
                console.log(err);
                return response.status(500).json({error: 'planStatus something wrong.'});
            }
        });
    }).catch((err) => {
        if (err) {
            console.log(err);
            return response.status(500).json({error: 'category something wrong.'});
        }
    });
    */
});
