'use strict';
const _ = require('lodash');

const config = require('../../../configs/config');
const Card = require(config.root + '/models/card');


exports.createItem = function(request, response, next) {
    const fields = request.body;
    const user = request.user;

    let processedFields = {};
    _.forOwn(fields, (value, key) => {
        processedFields[key] = value;
    });
    processedFields.owner = user;
    let card = new Card(processedFields);
    card.save((saveError, savedCard) => {
        if (saveError) {
            let errorMessage = saveError || {};
            errorMessage._status = 500;
            return next(errorMessage);
        }
        if (!savedCard) {
            let errorMessage = {};
            errorMessage._status = 500;
            return next(errorMessage);
        }

        request._cardSchema = savedCard;
        next();
    });
};

exports.sendResponse = function(request, response, next) {
    return response.status(201).json({data: request._cardSchema._doc});
};