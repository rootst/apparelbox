'use strict';
const _ = require('lodash');

const config = require('../../../configs/config');
const User = require(config.root + '/models/user');
const UserInformation = require(config.root + '/models/userInformation');
const Card = require(config.root + '/models/card');


exports.process = function(request, response, next) {
    let userPk = request.params.userPk;

    User.findById(userPk, (foundError, foundUser) => {
        if (foundError) {
            let errorMessage = foundError || {};
            errorMessage._status = 500;
            response.render('error', {error: errorMessage});
            return next();
        }
        if (!foundUser) {
            let errorMessage = foundError || {};
            errorMessage._status = 404;
            response.render('error', {error: errorMessage});
            return next();
        }

        UserInformation.find({owner: userPk}, (foundError, foundUserInformations) => {
            if (foundError) {
                let errorMessage = foundError || {};
                errorMessage._status = 500;
                response.render('error', {error: errorMessage});
                return next();
            }

            return response.render(
                'user/card',
                {target: foundUser, userInformations: foundUserInformations}
            );
        });
    });
};