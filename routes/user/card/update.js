'use strict';
const _ = require('lodash');

const config = require('../../../configs/config');
const stringContains = require(config.root + '/utilities/stringContains');
const Card = require(config.root + '/models/card');


exports.updateItem = function(request, response, next) {
    const cardPk = request.params.cardPk;
    const user = request.user;
    const fields = request.body;

    Card.findById(cardPk, (foundError, foundCard) => {
        if (foundError) {
            let errorMessage = foundError || {};
            errorMessage._status = 500;
            return next(errorMessage);
        }
        if (!foundCard) {
            let errorMessage = {};
            errorMessage._status = 404;
            return next(errorMessage);
        }
        // check authenticate
        if ((String(foundCard.owner) !== String(user._id)) &&
            (!stringContains(user.roles, config.administrators))) {
            let errorMessage = {};
            errorMessage._status = 403;
            return next(errorMessage);
        }

        _.forOwn(fields, (value, key) => {
            foundCard[key] = value;
        });

        foundCard.save((updateError, updatedCard) => {
            if (updateError) {
                let errorMessage = {};
                errorMessage._status = 404;
                return next(errorMessage);
            }
            if (!updatedCard) {
                let errorMessage = {};
                errorMessage._status = 404;
                return next(errorMessage);
            }

            request._cardSchema = updatedCard;
            next();
        });
    });
};

exports.sendResponse = function(request, response, next) {
    return response.status(200).json({data: request._cardSchema._doc});
};
