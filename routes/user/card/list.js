'use strict';
const _ = require('lodash');

const config = require('../../../configs/config');
const Card = require(config.root + '/models/card');


exports.getPaginatedItems = function(request, response, next) {
    let copiedQueryStatement = _.clone(request.query);
    let page = request.query.page;

    copiedQueryStatement = _.omit(copiedQueryStatement, 'page');

    if (_.isUndefined(page)) {
        page = 1;
    }

    let queryOptions = {
        page: page,
        limit: config.pagination.limit,
        sort: {createdDate: -1},
        populate: [
            {path: 'owner', select: '-password'}
        ]
    };
    Card.paginate(copiedQueryStatement,
                  queryOptions,
                  (paginationError, paginatedCards) => {
        if (paginationError) {
            let errorMessage = paginationError || {};
            errorMessage._status = 500;
            return next(errorMessage);
        }

        let hasMore = true;
        if (result.pages <= result.page) {
            hasMore = false;
        }

        request._cards = {
            total: result.total,
            hasMore: hasMore,
            items: result.docs,
            page: result.page
        };
        next();
    });
};

exports.postProcess = function(request, response, next) {
    _.forEach(request._cards.items, (card) => {
        //
    });
    next();
};

exports.sendResponse = function(request, response, next) {
    return response.status(200).json({data: request._cards});
};