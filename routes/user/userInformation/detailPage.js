'use strict';
const _ = require('lodash');

const config = require('../../../configs/config');
const User = require(config.root + '/models/user');
const UserInformation = require(config.root + '/models/userInformation');


exports.process = function(request, response, next) {
    let userPk = request.params.userPk;
    let userInformationPk = request.params.userInformationPk;

    User.findById(userPk, (foundError, foundUser) => {
        if (foundError) {
            let errorMessage = foundError || {};
            errorMessage._status = 500;
            response.render('error', {error: errorMessage});
            return next();
        }
        if (!foundUser) {
            let errorMessage = foundError || {};
            errorMessage._status = 404;
            response.render('error', {error: errorMessage});
            return next();
        }

        UserInformation.find({owner: userPk}, (foundError, foundUserInformations) => {
            if (foundError) {
                let errorMessage = foundError || {};
                errorMessage._status = 500;
                response.render('error', {error: errorMessage});
                return next();
            }

            UserInformation.findById(
                userInformationPk,
                (foundUserInformationError, foundUserInformation) => {
                
                return response.render(
                    'user/user_information', {
                        target: foundUser,
                        userInformations: foundUserInformations,
                        userInformation: foundUserInformation._doc
                    }
                );
            });
        });
    });
};