'use strict';
const _ = require('lodash');

const config = require('../../../configs/config');
const UserInformation = require(config.root + '/models/userInformation');


exports.createItem = function(request, response, next) {
    const fields = request.body;
    const user = request.user;

    let processedFields = {};
    _.forOwn(fields, (value, key) => {
        processedFields[key] = value;
    });
    processedFields.owner = user;
    let userInformation = new UserInformation(processedFields);
    userInformation.save((saveError, savedUserInformation) => {
        if (saveError) {
            let errorMessage = saveError || {};
            errorMessage._status = 500;
            return next(errorMessage);
        }
        if (!savedUserInformation) {
            let errorMessage = {};
            errorMessage._status = 500;
            return next(errorMessage);
        }

        request._userInformationSchema = savedUserInformation;
        next();
    });
};

exports.sendResponse = function(request, response, next) {
    return response.status(201).json({data: request._userInformationSchema._doc});
};