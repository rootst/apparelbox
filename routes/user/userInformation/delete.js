'use strict';
const config = require('../../../configs/config');
const stringContains = require(config.root + '/utilities/stringContains');
const UserInformation = require(config.root + '/models/userInformation');


exports.deleteItem = function(request, response, next) {
    const userInformationPk = request.params.userInformationPk;
    const user = request.user;

    UserInformation.findById(userInformationPk, (foundError, foundUserInformation) => {
        if (foundError) {
            let errorMessage = foundError || {};
            errorMessage._status = 500;
            return next(errorMessage);
        }
        if (!foundUserInformation) {
            let errorMessage = {};
            errorMessage._status = 404;
            return next(errorMessage);
        }
        // check authenticate
        if ((String(foundUserInformation.owner) !== String(user._id)) &&
            (!stringContains(user.roles, config.administrators))) {
            let errorMessage = {};
            errorMessage._status = 403;
            return next(errorMessage);
        }

        foundUserInformation.remove((removeError) => {
            if (removeError) {
                let errorMessage = {};
                errorMessage._status = 500;
                return next(errorMessage);
            }

            next();
        });
    });
};

exports.sendResponse = function(request, response, next) {
    return response.status(204).json({});
};