'use strict';
const _ = require('lodash');

const config = require('../../../configs/config');
const stringContains = require(config.root + '/utilities/stringContains');
const UserInformation = require(config.root + '/models/userInformation');


exports.updateItem = function(request, response, next) {
    const userInformationPk = request.params.userInformationPk;
    const user = request.user;
    const fields = request.body;

    UserInformation.findById(userInformationPk, (foundError, foundUserInformation) => {
        if (foundError) {
            let errorMessage = foundError || {};
            errorMessage._status = 500;
            return next(errorMessage);
        }
        if (!foundUserInformation) {
            let errorMessage = {};
            errorMessage._status = 404;
            return next(errorMessage);
        }
        // check authenticate
        if ((String(foundUserInformation.owner) !== String(user._id)) &&
            (!stringContains(user.roles, config.administrators))) {
            let errorMessage = {};
            errorMessage._status = 403;
            return next(errorMessage);
        }

        _.forOwn(fields, (value, key) => {
            /*
            if (_.indexOf(key, '.') !== -1) { // ex) size.shirt
                let keys = _.split(key, '.');
                if (keys[1] === 'inseam') {
                    foundUserInformation[_.toString(keys[0])] = value;
                } else {
                    if (_.isUndefined(foundUserInformation[_.toString(keys[0])])) {
                        foundUserInformation[_.toString(keys[0])] = {};
                    }
                    foundUserInformation[_.toString(keys[0])][_.toString(keys[1])] =
                        config[_.toString(keys[0])][_.toString(keys[1])][_.toNumber(value)].content;
                }
            } else {
                if (key === 'dateOfBirth') {
                    value = '1990-01-13';
                    foundUserInformation[key] = new Date(value);
                } else {
                    foundUserInformation[key] = value;
                }
            }
            */
            if (_.indexOf(key, '.') !== -1) { // ex) size.shirt
                let keys = _.split(key, '.');
                if (_.isUndefined(foundUserInformation[keys[0]])) {
                    foundUserInformation[keys[0]] = {};
                }
                foundUserInformation[keys[0]][keys[1]] = value;
            } else {
                if (key === 'dateOfBirth') {
                    value = '1990-01-13';
                    foundUserInformation[key] = new Date(value);
                } else {
                    foundUserInformation[key] = value;
                }
            }
        });

        foundUserInformation.save((updateError, updatedUserInformation) => {
            if (updateError) {
                updateError._status = 500;
                return next(updateError);
            }
            if (!updatedUserInformation) {
                let errorMessage = {};
                errorMessage._status = 404;
                return next(errorMessage);
            }

            request._userInformationSchema = updatedUserInformation;
            next();
        });
    });
};

exports.sendResponse = function(request, response, next) {
    return response.status(200).json({data: request._userInformationSchema._doc});
};
