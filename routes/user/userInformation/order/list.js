'use strict';
const _ = require('lodash');

const config = require('../../../../configs/config');
const Order = require(config.root + '/models/order');


exports.getPaginatedItems = function(request, response, next) {
    let copiedQueryStatement = _.clone(request.query);
    let page = request.query.page;

    copiedQueryStatement = _.omit(copiedQueryStatement, 'page');

    if (_.isUndefined(page)) {
        page = 1;
    }

    let queryOptions = {
        page: page,
        limit: config.pagination.limit,
        sort: {createdDate: -1},
        populate: [
            {path: 'owner'},
            {path: 'userInformation'}
        ]
    };
    Order.paginate(copiedQueryStatement,
                   queryOptions,
                   (paginationError, paginatedOrders) => {
        if (paginationError) {
            let errorMessage = paginationError || {};
            errorMessage._status = 500;
            return next(errorMessage);
        }

        let hasMore = true;
        if (paginatedOrders.pages <= paginatedOrders.page) {
            hasMore = false;
        }

        request._orders = {
            total: paginatedOrders.total,
            hasMore: hasMore,
            items: paginatedOrders.docs,
            page: paginatedOrders.page
        };
        next();
    });
};

exports.postProcess = function(request, response, next) {
    _.forEach(request._orders.items, (order) => {
        //
    });
    next();
};

exports.sendResponse = function(request, response, next) {
    return response.status(200).json({data: request._orders});
};