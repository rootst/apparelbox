'use strict';
const _ = require('lodash');

const config = require('../../../../configs/config');
const Order = require(config.root + '/models/order');
const UserInformation = require(config.root + '/models/userInformation');


exports.createItem = function(request, response, next) {
    const fields = request.body;
    const user = request.user;
    const userInformationPk = request.params.userInformationPk;

    UserInformation.findById(userInformationPk, (foundError, foundUserInformation) => {
        if (foundError) {
            let errorMessage = foundError || {};
            errorMessage._status = 500;
            return next(errorMessage);
        }
        if (!foundUserInformation) {
            let errorMessage = {};
            errorMessage._status = 404;
            return next(errorMessage);
        }
        // check authenticate
        if ((String(foundUserInformation.owner) !== String(user._id)) &&
            (!stringContains(user.roles, config.administrators))) {
            let errorMessage = {};
            errorMessage._status = 403;
            return next(errorMessage);
        }

        let processedFields = {};
        _.forOwn(fields, (value, key) => {
            processedFields[key] = value;
        });
        processedFields.owner = user;
        processedFields.userInformation = foundUserInformation;
        processedFields.status = config.orderStatus.receipted;
        let order = new Order(processedFields);
        order.save((saveError, savedOrder) => {
            if (saveError) {
                let errorMessage = saveError || {};
                errorMessage._status = 500;
                return next(errorMessage);
            }
            if (!savedOrder) {
                let errorMessage = {};
                errorMessage._status = 500;
                return next(errorMessage);
            }

            request._orderSchema = savedOrder;
            next();
        });
    });
};

exports.sendResponse = function(request, response, next) {
    return response.status(201).json({data: request._orderSchema._doc});
};