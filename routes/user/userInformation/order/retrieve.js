'use strict';
const config = require('../../../../configs/config');
const stringContains = require(config.root + '/utilities/stringContains');
const Order = require(config.root + '/models/order');


exports.findItem = function(request, response, next) {
    const orderPk = request.params.orderPk;
    const user = request.user;

    Order.findById(orderPk, (foundError, foundOrder) => {
        if (foundError) {
            let errorMessage = foundError || {};
            errorMessage._status = 500;
            return next(errorMessage);
        }
        if (!foundOrder) {
            let errorMessage = {};
            errorMessage._status = 404;
            return next(errorMessage);
        }
        // check authenticate
        if ((String(foundOrder.owner) !== String(user._id)) &&
            (!stringContains(user.roles, config.administrators))) {
            let errorMessage = {};
            errorMessage._status = 403;
            return next(errorMessage);
        }

        request._orderSchema = foundOrder;
        next();
    });
};

exports.postProcess = function(request, response, next) {
    next();
};

exports.sendResponse = function(request, response, next) {
    return response.status(200).json({data: request._orderSchema._doc});
};