'use strict';
const _ = require('lodash');

const config = require('../../configs/config');
const User = require(config.root + '/models/user');


exports.getPaginatedItems = function(request, response, next) {
    let copiedQueryStatement = _.clone(request.query);
    let page = request.query.page;

    copiedQueryStatement = _.omit(copiedQueryStatement, 'page');

    if (_.isUndefined(page)) {
        page = 1;
    }

    let queryOptions = {
        page: page,
        limit: config.pagination.limit,
        sort: {joinedDate: -1},
        select: '-password'
    };
    User.paginate(copiedQueryStatement,
                  queryOptions,
                  (paginationError, paginatedUsers) => {
        if (paginationError) {
            let errorMessage = paginationError || {};
            errorMessage._status = 500;
            return next(errorMessage);
        }

        let hasMore = true;
        if (paginatedUsers.pages <= paginatedUsers.page) {
            hasMore = false;
        }

        request._users = {
            total: paginatedUsers.total,
            hasMore: hasMore,
            items: paginatedUsers.docs,
            page: paginatedUsers.page
        };
        next();
    });
};

exports.postProcess = function(request, response, next) {
    _.forEach(request._users.items, (user) => {
        
    });
    next();
};

exports.sendResponse = function(request, response, next) {
    return response.status(200).json({data: request._users});
};