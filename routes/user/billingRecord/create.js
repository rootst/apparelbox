'use strict';
const _ = require('lodash');

const config = require('../../../configs/config');
const BillingRecord = require(config.root + '/models/billingRecord');


exports.createItem = function(request, response, next) {
    const fields = request.body;
    const user = request.user;

    let processedFields = {};
    _.forOwn(fields, (value, key) => {
        processedFields[key] = value;
    });
    let billingRecord = new BillingRecord(processedFields);
    billingRecord.save((saveError, savedBillingRecord) => {
        if (saveError) {
            let errorMessage = saveError || {};
            errorMessage._status = 500;
            return next(errorMessage);
        }
        if (!savedBillingRecord) {
            let errorMessage = {};
            errorMessage._status = 500;
            return next(errorMessage);
        }

        request._billingRecordSchema = savedBillingRecord;
        next();
    });
};

exports.sendResponse = function(request, response, next) {
    return response.status(201).json({data: request._billingRecordSchema._doc});
};