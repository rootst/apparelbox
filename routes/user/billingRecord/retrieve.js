'use strict';
const config = require('../../../configs/config');
const stringContains = require(config.root + '/utilities/stringContains');
const BillingRecord = require(config.root + '/models/billingRecord');


exports.findItem = function(request, response, next) {
    const billingRecordPk = request.params.billingRecordPk;
    const user = request.user;

    BillingRecord.findById(billingRecordPk, (foundError, foundBillingRecord) => {
        if (foundError) {
            let errorMessage = foundError || {};
            errorMessage._status = 500;
            return next(errorMessage);
        }
        if (!foundBillingRecord) {
            let errorMessage = {};
            errorMessage._status = 404;
            return next(errorMessage);
        }
        // check authenticate
        if ((String(foundBillingRecord.owner) !== String(user._id)) &&
            (!stringContains(user.roles, config.administrators))) {
            let errorMessage = {};
            errorMessage._status = 403;
            return next(errorMessage);
        }

        request._billingRecordSchema = foundBillingRecord;
        next();
    });
};

exports.postProcess = function(request, response, next) {
    next();
};

exports.sendResponse = function(request, response, next) {
    return response.status(200).json({data: request._billingRecordSchema._doc});
};