'use strict';
const _ = require('lodash');

const config = require('../../../configs/config');
const stringContains = require(config.root + '/utilities/stringContains');
const BillingRecord = require(config.root + '/models/billingRecord');


exports.updateItem = function(request, response, next) {
    const billingRecordPk = request.params.billingRecordPk;
    const user = request.user;
    const fields = request.body;

    BillingRecord.findById(billingRecordPk, (foundError, foundBillingRecord) => {
        if (foundError) {
            let errorMessage = foundError || {};
            errorMessage._status = 500;
            return next(errorMessage);
        }
        if (!foundBillingRecord) {
            let errorMessage = {};
            errorMessage._status = 404;
            return next(errorMessage);
        }
        // check authenticate
        if ((String(foundBillingRecord.owner) !== String(user._id)) &&
            (!stringContains(user.roles, config.administrators))) {
            let errorMessage = {};
            errorMessage._status = 403;
            return next(errorMessage);
        }

        _.forOwn(fields, (value, key) => {
            foundBillingRecord[key] = value;
        });

        foundBillingRecord.save((updateError, updatedBillingRecord) => {
            if (updateError) {
                let errorMessage = {};
                errorMessage._status = 404;
                return next(errorMessage);
            }
            if (!updatedBillingRecord) {
                let errorMessage = {};
                errorMessage._status = 404;
                return next(errorMessage);
            }

            request._billingRecordSchema = updatedBillingRecord;
            next();
        });
    });
};

exports.sendResponse = function(request, response, next) {
    return response.status(200).json({data: request._billingRecordSchema._doc});
};
