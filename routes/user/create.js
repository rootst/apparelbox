'use strict';
const _ = require('lodash');
const passport = require('passport');

const config = require('../../configs/config');
const User = require(config.root + '/models/user');


exports.createItem = function(request, response, next) {
    const fields = request.body;

    let processedFields = {};
    _.forOwn(fields, (value, key) => {
        processedFields[key] = value;
    });
    processedFields.roles = config.userRole.clients;
    let user = new User(processedFields);
    user.save((saveError, savedUser) => {
        if (saveError) {
            let errorMessage = saveError || {};
            errorMessage._status = 500;
            return next(errorMessage);
        }
        if (!savedUser) {
            let errorMessage = {};
            errorMessage._status = 500;
            return next(errorMessage);
        }

        request._userSchema = savedUser;
        next();
    });
};

exports.authenticate = function(request, response, next) {
    request.login(request._userSchema, (loggedInError) => {
        if (loggedInError) {
            let errorMessage = loggedInError || {};
            errorMessage._status = 401;
            return next(errorMessage);
        }
        next();
    });
};

exports.sendResponse = function(request, response, next) {
    return response.status(201).json({data: request._userSchema._doc});
};
