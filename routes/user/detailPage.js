'use strict';
const _ = require('lodash');

const config = require('../../configs/config');
const User = require(config.root + '/models/user');
const UserInformation = require(config.root + '/models/userInformation');


exports.process = function(request, response, next) {
    let userPk = request.params.userPk;

    User.findById(userPk, (foundError, foundUser) => {
        if (foundError) {
            let errorMessage = foundError || {};
            errorMessage._status = 500;
            return response.render('error', {error: errorMessage});
        }
        if (!foundUser) {
            let errorMessage = foundError || {};
            errorMessage._status = 404;
            return response.render('error', {error: errorMessage});
        }

        // first, lets find this user is supervisor or not.
        // if then, we don't need to find userinformation.
        _.forEach(config.userRole.administrators, (administrator) => {
            if (_.indexOf(foundUser.roles, administrator) !== -1) {
                return response.render('supervisor/administrator/index');
            }
        });
        _.forEach(config.userRole.clerks, (clerk) => {
            if (_.indexOf(foundUser.roles, clerk) !== -1) {
                return response.render('supervisor/index');
            }
        });

        UserInformation.find({owner: userPk}, (foundError, foundUserInformations) => {
            if (foundError) {
                let errorMessage = foundError || {};
                errorMessage._status = 500;
                return response.render('error', {error: errorMessage});
            }
            return response.render('user/profile', {target: foundUser, userInformations: foundUserInformations, mypage_default: 'on'});
        });
    });
};