'use strict';
const _ = require('lodash');

const config = require('../../configs/config');
const stringContains = require(config.root + '/utilities/stringContains');
const User = require(config.root + '/models/user');


exports.findItem = function(request, response, next) {
    const userPk = request.params.userPk;
    const user = request.user;

    let queryOptions = {
        select: '-password'
    };
    User.findById(userPk, queryOptions, (foundError, foundUser) => {
        if (foundError) {
            let errorMessage = foundError || {};
            errorMessage._status = 500;
            return next(errorMessage);
        }
        if (!foundUser) {
            let errorMessage = {};
            errorMessage._status = 404;
            return next(errorMessage);
        }
        // check authenticate
        if ((String(foundUser._id) !== String(user._id)) &&
            (!stringContains(user.roles, config.administrators))) {
            let errorMessage = {};
            errorMessage._status = 403;
            return next(errorMessage);
        }

        request._userSchema = foundUser;
        next();
    });
};

exports.postProcess = function(request, response, next) {
    next();
};

exports.sendResponse = function(request, response, next) {
    return response.status(200).json({data: request._userSchema._doc});
};