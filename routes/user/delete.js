'use strict';
const config = require('../../configs/config');
const stringContains = require(config.root + '/utilities/stringContains');
const User = require(config.root + '/models/user');


exports.deleteItem = function(request, response, next) {
    const userPk = request.params.userPk;
    const user = request.user;

    User.findById(userPk, (foundError, foundUser) => {
        if (foundError) {
            let errorMessage = foundError || {};
            errorMessage._status = 500;
            return next(errorMessage);
        }
        if (!foundUser) {
            let errorMessage = {};
            errorMessage._status = 404;
            return next(errorMessage);
        }
        // check authenticate
        if ((String(foundUser._id) !== String(user._id)) &&
            (!stringContains(user.roles, config.administrators))) {
            let errorMessage = {};
            errorMessage._status = 403;
            return next(errorMessage);
        }

        foundUser.remove((removeError) => {
            if (removeError) {
                let errorMessage = {};
                errorMessage._status = 500;
                return next(errorMessage);
            }

            next();
        });
    });
};

exports.sendResponse = function(request, response, next) {
    return response.status(204).json({});
};