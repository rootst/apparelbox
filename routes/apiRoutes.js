'use strict';
const express = require('express');
const passport = require('passport');
const _ = require('lodash');

const config = require('../configs/config');
const userJudge = require(config.root + '/utilities/userJudge');
const authenticationChecker =
    require(config.root + '/utilities/authenticationChecker');

const userList = require(config.root + '/routes/user/list');
const userCreate = require(config.root + '/routes/user/create');
const userRetrieve = require(config.root + '/routes/user/retrieve');
const userUpdate = require(config.root + '/routes/user/update');
const userDelete = require(config.root + '/routes/user/delete');

const cardList = require(config.root + '/routes/user/card/list');
const cardCreate = require(config.root + '/routes/user/card/create');
const cardRetrieve = require(config.root + '/routes/user/card/retrieve');
const cardUpdate = require(config.root + '/routes/user/card/update');
const cardDelete = require(config.root + '/routes/user/card/delete');

const userInformationList =
    require(config.root + '/routes/user/userInformation/list');
const userInformationCreate =
    require(config.root + '/routes/user/userInformation/create');
const userInformationRetrieve =
    require(config.root + '/routes/user/userInformation/retrieve');
const userInformationUpdate =
    require(config.root + '/routes/user/userInformation/update');
const userInformationDelete =
    require(config.root + '/routes/user/userInformation/delete');
    
const orderList = require(config.root + '/routes/user/userInformation/order/list');
const orderCreate = require(config.root + '/routes/user/userInformation/order/create');
const orderRetrieve = require(config.root + '/routes/user/userInformation/order/retrieve');
const orderUpdate = require(config.root + '/routes/user/userInformation/order/update');
const orderDelete = require(config.root + '/routes/user/userInformation/order/delete');


const router = express.Router();


router.get('/users',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clerks,
        config.userRole.clients)
    ),
    userList.getPaginatedItems,
    userList.postProcess,
    userList.sendResponse
);
router.post('/users',
    userCreate.createItem,
    userCreate.authenticate,
    userCreate.sendResponse
);
router.get('/users/:userPk',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clerks,
        config.userRole.clients)
    ),
    userRetrieve.findItem,
    userRetrieve.postProcess,
    userRetrieve.sendResponse
);
router.put('/users/:userPk',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clients)
    ),
    userUpdate.updateItem,
    userUpdate.sendResponse
);
router.patch('/users/:userPk',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clients)
    ),
    userUpdate.updateItem,
    userUpdate.sendResponse
);
router.delete('/users/:userPk',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clients)
    ),
    userDelete.deleteItem,
    userDelete.sendResponse
);

router.get('/users/:userPk/cards',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clients)
    ),
    cardList.getPaginatedItems,
    cardList.postProcess,
    cardList.sendResponse
);
router.post('/users/:userPk/cards',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clients)
    ),
    cardCreate.createItem,
    cardCreate.sendResponse
);
router.get('/users/:userPk/cards/:cardPk',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clients)
    ),
    cardRetrieve.findItem,
    cardRetrieve.postProcess,
    cardRetrieve.sendResponse
);
router.put('/users/:userPk/cards/:cardPk',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clients)
    ),
    cardUpdate.updateItem,
    cardUpdate.sendResponse
);
router.patch('/users/:userPk/cards/:cardPk',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clients)
    ),
    cardUpdate.updateItem,
    cardUpdate.sendResponse
);
router.delete('/users/:userPk/cards/:cardPk',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clients)
    ),
    cardDelete.deleteItem,
    cardDelete.sendResponse
);


router.get('/users/:userPk/user_informations',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clerks,
        config.userRole.clients)
    ),
    userInformationList.getPaginatedItems,
    userInformationList.postProcess,
    userInformationList.sendResponse
);
router.post('/users/:userPk/user_informations',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clients)
    ),
    userInformationCreate.createItem,
    userInformationCreate.sendResponse
);
router.get('/users/:userPk/user_informations/:userInformationPk',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clerks,
        config.userRole.clients)
    ),
    userInformationRetrieve.findItem,
    userInformationRetrieve.postProcess,
    userInformationRetrieve.sendResponse
);
router.put('/users/:userPk/user_informations/:userInformationPk',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clerks,
        config.userRole.clients)
    ),
    userInformationUpdate.updateItem,
    userInformationUpdate.sendResponse
);
router.patch('/users/:userPk/user_informations/:userInformationPk',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clerks,
        config.userRole.clients)
    ),
    userInformationUpdate.updateItem,
    userInformationUpdate.sendResponse
);
router.delete('/users/:userPk/user_informations/:userInformationPk',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clients)
    ),
    userInformationDelete.deleteItem,
    userInformationDelete.sendResponse
);

router.get('/users/:userPk/user_informations/:userInformationPk/orders',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clerks,
        config.userRole.clients)
    ),
    orderList.getPaginatedItems,
    orderList.postProcess,
    orderList.sendResponse
);
router.post('/users/:userPk/user_informations/:userInformationPk/orders',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clients)
    ),
    orderCreate.createItem,
    orderCreate.sendResponse
);
router.get('/users/:userPk/user_informations/:userInformationPk/orders/:orderPk',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clerks,
        config.userRole.clients)
    ),
    orderRetrieve.findItem,
    orderRetrieve.postProcess,
    orderRetrieve.sendResponse
);
router.put('/users/:userPk/user_informations/:userInformationPk/orders/:orderPk',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clerks,
        config.userRole.clients)
    ),
    orderUpdate.updateItem,
    orderUpdate.sendResponse
);
router.patch('/users/:userPk/user_informations/:userInformationPk/orders/:orderPk',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clerks,
        config.userRole.clients)
    ),
    orderUpdate.updateItem,
    orderUpdate.sendResponse
);
router.delete('/users/:userPk/user_informations/:userInformationPk/orders/:orderPk',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clients)
    ),
    orderDelete.deleteItem,
    orderDelete.sendResponse
);


/* ADMINISTRATOR, clerks ONLY START */

const inventoryList = require(config.root + '/routes/inventory/list');
const inventoryCreate = require(config.root + '/routes/inventory/create');
const inventoryRetrieve = require(config.root + '/routes/inventory/retrieve');
const inventoryUpdate = require(config.root + '/routes/inventory/update');
const inventoryDelete = require(config.root + '/routes/inventory/delete');

router.get('/inventories',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clerks)
    ),
    inventoryList.getPaginatedItems,
    inventoryList.postProcess,
    inventoryList.sendResponse
);
router.post('/inventories',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clerks)
    ),
    inventoryCreate.createItem,
    inventoryCreate.sendResponse
);
router.get('/inventories/:inventoryPk',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clerks)
    ),
    inventoryRetrieve.findItem,
    inventoryRetrieve.postProcess,
    inventoryRetrieve.sendResponse
);
router.put('/inventories/:inventoryPk',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clerks)
    ),
    inventoryUpdate.updateItem,
    inventoryUpdate.sendResponse
);
router.patch('/inventories/:inventoryPk',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators,
        config.userRole.clerks)
    ),
    inventoryUpdate.updateItem,
    inventoryUpdate.sendResponse
);
router.delete('/inventories/:inventoryPk',
    authenticationChecker,
    userJudge.requireRole(_.concat(
        config.userRole.administrators)
    ),
    inventoryDelete.deleteItem,
    inventoryDelete.sendResponse
);

/* ADMINISTRATOR ONLY END */

module.exports = router;
