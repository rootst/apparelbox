'use strict';
const _ = require('lodash');

const config = require('../../configs/config');
const Inventory = require(config.root + '/models/inventory');


exports.getPaginatedItems = function(request, response, next) {
    let copiedQueryStatement = _.clone(request.query);
    let page = request.query.page;

    copiedQueryStatement = _.omit(copiedQueryStatement, 'page');

    if (_.isUndefined(page)) {
        page = 1;
    }

    let queryOptions = {
        page: page,
        limit: config.pagination.limit,
        sort: {createdDate: -1}
    };
    Inventory.paginate(copiedQueryStatement,
                       queryOptions,
                       (paginationError, paginatedInventorys) => {
        if (paginationError) {
            let errorMessage = paginationError || {};
            errorMessage._status = 500;
            return next(errorMessage);
        }

        let hasMore = true;
        if (result.pages <= result.page) {
            hasMore = false;
        }

        request._inventories = {
            total: result.total,
            hasMore: hasMore,
            items: result.docs,
            page: result.page
        };
        next();
    });
};

exports.postProcess = function(request, response, next) {
    _.forEach(request._inventories.items, (inventory) => {
        //
    });
    next();
};

exports.sendResponse = function(request, response, next) {
    return response.status(200).json({data: request._inventories});
};