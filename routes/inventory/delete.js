'use strict';
const config = require('../../configs/config');
const stringContains = require(config.root + '/utilities/stringContains');
const Inventory = require(config.root + '/models/inventory');


exports.deleteItem = function(request, response, next) {
    const inventoryPk = request.params.inventoryPk;
    const user = request.user;

    Inventory.findById(inventoryPk, (foundError, foundInventory) => {
        if (foundError) {
            let errorMessage = foundError || {};
            errorMessage._status = 500;
            return next(errorMessage);
        }
        if (!foundInventory) {
            let errorMessage = {};
            errorMessage._status = 404;
            return next(errorMessage);
        }
        // check authenticate
        if ((String(foundInventory.owner) !== String(user._id)) &&
            (!stringContains(user.roles, config.administrators))) {
            let errorMessage = {};
            errorMessage._status = 403;
            return next(errorMessage);
        }

        foundInventory.remove((removeError) => {
            if (removeError) {
                let errorMessage = {};
                errorMessage._status = 500;
                return next(errorMessage);
            }

            next();
        });
    });
};

exports.sendResponse = function(request, response, next) {
    return response.status(204).json({});
};