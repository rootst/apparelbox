'use strict';
const _ = require('lodash');

const config = require('../../configs/config');
const stringContains = require(config.root + '/utilities/stringContains');
const Inventory = require(config.root + '/models/inventory');


exports.updateItem = function(request, response, next) {
    const inventoryPk = request.params.inventoryPk;
    const user = request.user;
    const fields = request.body;

    Inventory.findById(inventoryPk, (foundError, foundInventory) => {
        if (foundError) {
            let errorMessage = foundError || {};
            errorMessage._status = 500;
            return next(errorMessage);
        }
        if (!foundInventory) {
            let errorMessage = {};
            errorMessage._status = 404;
            return next(errorMessage);
        }
        // check authenticate
        if ((String(foundInventory.owner) !== String(user._id)) &&
            (!stringContains(user.roles, config.administrators))) {
            let errorMessage = {};
            errorMessage._status = 403;
            return next(errorMessage);
        }

        _.forOwn(fields, (value, key) => {
            foundInventory[key] = value;
        });

        foundInventory.save((updateError, updatedInventory) => {
            if (updateError) {
                let errorMessage = {};
                errorMessage._status = 404;
                return next(errorMessage);
            }
            if (!updatedInventory) {
                let errorMessage = {};
                errorMessage._status = 404;
                return next(errorMessage);
            }

            request._inventorySchema = updatedInventory;
            next();
        });
    });
};

exports.sendResponse = function(request, response, next) {
    return response.status(200).json({data: request._inventorySchema._doc});
};
