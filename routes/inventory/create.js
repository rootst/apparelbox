'use strict';
const _ = require('lodash');

const config = require('../../configs/config');
const Inventory = require(config.root + '/models/inventory');


exports.createItem = function(request, response, next) {
    const fields = request.body;

    let processedFields = {};
    _.forOwn(fields, (value, key) => {
        processedFields[key] = value;
    });
    let inventory = new Inventory(processedFields);
    inventory.save((saveError, savedInventory) => {
        if (saveError) {
            let errorMessage = saveError || {};
            errorMessage._status = 500;
            return next(errorMessage);
        }
        if (!savedInventory) {
            let errorMessage = {};
            errorMessage._status = 500;
            return next(errorMessage);
        }

        request._inventorySchema = savedInventory;
        next();
    });
};

exports.sendResponse = function(request, response, next) {
    return response.status(201).json({data: request._inventorySchema._doc});
};