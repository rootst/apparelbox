'use strict';
const _ = require('lodash');
const express = require('express');
const passport = require('passport');

const config = require('../configs/config');
const authenticationChecker =
    require(config.root + '/utilities/authenticationChecker');
const userNewPage =
    require(config.root + '/routes/user/newPage');
const userDetailPage =
    require(config.root + '/routes/user/detailPage');
const userInformationNewPage =
    require(config.root + '/routes/user/userInformation/newPage');
const userInformationDetailPage =
    require(config.root + '/routes/user/userInformation/detailPage');
const cardListPage =
    require(config.root + '/routes/user/card/listPage');
const billingRecordListPage =
    require(config.root + '/routes/user/billingRecord/listPage');


const UserInformation = require(config.root + '/models/userInformation');


const router = express.Router();

/* SNS AUTHENTICATION BEGIN */

router.get('/auth/facebook', 
    passport.authenticate('facebook', {scope: ['email']})
);
router.get('/auth/facebook/callback',
    passport.authenticate('facebook', {
        failureRedirect: '/login_fail',
        failureFlash: true
    }), (request, response, next) => {
        if (request.user) {
            UserInformation.find({owner:request.user._id})
            .then((foundUserInformation) => {
                if (_.isEmpty(foundUserInformation)) {
                    return response.redirect(
                        '/users/' + request.user._id + '/user_informations/new'
                    );
                }

                response.redirect('/');

            }).catch((foundError) => {
                if (foundError) {
                    return response.json({data: '/'});
                }
            });
        } else {
            response.redirect(
                '/login_fail'
            );
        }
    }
);
router.get('/auth/kakao', 
    passport.authenticate('kakao', {})
);
router.get('/auth/kakao/callback',
    passport.authenticate('kakao', {
        failureRedirect: '/login_fail',
        failureFlash: true
    }), (request, response, next) => {
        if (request.user) {
            UserInformation.find({owner:request.user._id})
            .then((foundUserInformation) => {
                if (_.isEmpty(foundUserInformation)) {
                    return response.redirect(
                        '/users/' + request.user._id + '/user_informations/new'
                    );
                }

                response.redirect('/');

            }).catch((foundError) => {
                if (foundError) {
                    return response.json({data: '/'});
                }
            });
        } else {
            response.redirect(
                '/login_fail'
            );
        }
    }
);
router.get('/login_fail',
    (request, response, next) => {
        return response.render('socialAuthError', {error: request.flash('error')[0]});
    }
);

/* SNS AUTHENTICATION END */
function getUserInformation(request, response, next) {
    if (request.isAuthenticated()) {
        UserInformation
            .find({owner: request.user})
            .exec(function(err, ui) {
                if (err) {
                    let errorMessage = err || {};
                    errorMessage._status = 500;
                    return next(errorMessage);
                }
                request._userInformationList = ui;
                return next();
            });
    } else {
        return next();
    }
}
router.get('/',
    getUserInformation,
    (request, response, next) => {
        response.render('index', {ui: request._userInformationList});
        next();
    }
);
router.get('/login',
    (request, response, next) => {
        return response.render('login');
    }
);
router.post('/login',
    passport.authenticate('local-login', {
        failureRedirect: '/login_fail',
        failureFlash: true
    }), (request, response, next) => {
        if (request.user) {
            UserInformation.find({owner: request.user._id})
            .then((foundUserInformation) => {
                if (_.isEmpty(foundUserInformation)) {
                    return response.json({data:
                        '/users/' + request.user._id + '/user_informations/new'
                    });
                }

                response.json({data: '/'});

            }).catch((foundError) => {
                if (foundError) {
                    return response.json({data: '/'});
                }
            });
        } else {
            response.json({data: '/login_fail'});
        }
    }
);
router.get('/logout',
    (request, response, next) => {
        request.logout();
        return response.redirect('/');
    }
);
router.get('/apparelbox',
    (request, response, next) => {
        return response.render('apparelbox');
    }
);

// NOTE : POSITION WARNING! Should be above of detail page.
router.get('/users/new',
    userNewPage.process
);
router.get('/users/:userPk',
    authenticationChecker,
    userDetailPage.process
);
router.get('/users/:userPk/cards',
    authenticationChecker,
    cardListPage.process
);
router.get('/users/:userPk/billing_records',
    authenticationChecker,
    billingRecordListPage.process
);
router.get('/users/:userPk/user_informations/new',
    authenticationChecker,
    userInformationNewPage.process
);
router.get('/users/:userPk/user_informations/:userInformationPk',
    authenticationChecker,
    userInformationDetailPage.process
);


module.exports = router;
