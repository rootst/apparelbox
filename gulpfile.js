const pump = require('pump'); // for error log easier
const gulp = require('gulp');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const babel = require('gulp-babel'); // for compile es6
const uglify = require('gulp-uglify');
const sass = require('gulp-sass');
const jade = require('gulp-jade');
const livereload = require('gulp-livereload');
const connect = require('gulp-connect');
const nodemon = require('gulp-nodemon');

const config = require('./configs/config');

const src = 'public';
const dist = '.dist';

const paths = {
	jsVendor: [
		src + '/js/vendor/**/*.js',
		src + '/js/vendor/**/*.min.js',
		src + '/js/vendor/**/*.min.js.map'
	],
	jsOwn: src + '/js/0_main/*.js',
	scss: src + '/scss/**/*.scss',
	jade: src + '/views/**/*.jade',
	font: src + '/fonts/*',
	image: src + '/images/**/*',
};


// 파일 변경 감지 및 브라우저 재시작
gulp.task('watch', () => {
	gulp.watch(paths.js, ['combine-js']);
	gulp.watch(paths.scss, ['compile-sass']);
	gulp.watch(paths.jade, ['compile-jade']);
	gulp.watch(paths.jsVendor, ['copy-javascript']);
	gulp.watch(paths.image, ['copy-images']);
	gulp.watch(paths.font, ['copy-font']);
	//gulp.watch(dist + '/**/*').on('change', livereload.changed);
});

// 자바스크립트 파일을 하나로 합치고 압축한다.
gulp.task('combine-js', function (cb) {
	pump([
		gulp.src(paths.jsOwn),
		//concat('script.js'),
		gulp.dest(dist + '/js/0_main'),
		babel({presets: ['es2015']}),
		//rename('script.min.js'),
		uglify(),
		gulp.dest(dist + '/js/0_main'),
		connect.reload()
	], cb);
});

// sass 파일을 css 로 컴파일한다.
gulp.task('compile-sass', function (cb) {
	pump([
		gulp.src(paths.scss),
		sass(),
		gulp.dest(dist + '/css'),
		connect.reload()
	], cb);
});

// JADE 컴파일 
gulp.task('compile-jade', function(cb) {
	pump([
		gulp.src(paths.jade),
		jade({pretty: true}),
		gulp.dest(dist + '/'),
		connect.reload()
	], cb);
});

// jade 파일을 카피한다.
gulp.task('copy-jade', function (cb) {
	pump([
		gulp.src(paths.jade),
		gulp.dest(dist + '/views'),
		connect.reload()
	], cb)
});

// font 파일을 카피한다.
gulp.task('copy-font', function (cb) {
	pump([
		gulp.src(paths.font),
		gulp.dest(dist + '/fonts'),
		connect.reload()
	], cb)
});

// javascript 파일을 카피한다.
gulp.task('copy-javascript', function (cb) {
	pump([
		gulp.src(paths.jsVendor),
		gulp.dest(dist + '/js/vendor'),
		connect.reload()
	], cb)
});

// images 파일을 카피한다.
gulp.task('copy-images', function (cb) {
	pump([
		gulp.src(paths.image),
		gulp.dest(dist + '/images'),
		connect.reload()
	], cb)
});

//기본 task 설정
gulp.task('build', [
	'combine-js',
	'copy-javascript',
	'compile-sass',
	'compile-jade',
	'copy-font',
	'copy-images'
]);

// 웹서버를 localhost:3000 로 실행한다.
gulp.task('dev', ['build', 'watch'], () => {
	connect.server({
		root: dist,
		livereload: true,
		debug: true,
		port: 3000
	})
	/*
	livereload.listen();
	let stream = nodemon({
		script: 'server.js',
		env: {'NODE_ENV': 'development'},
		watch: '*.*',
		ignore: [dist + '/']
	});

	stream.on('start', () => {
		console.log('started!');
	}).on('restart', () => {
        console.log('restarted!');
	}).on('crash', () => {
		console.error('Application has crashed!\n');
		stream.emit('restart', 10);// restart the server in 10 seconds 
	});
	*/
});

gulp.task('prod', [], () => {
	let stream = nodemon({
		script: 'server.js',
		env: {NODE_ENV: 'production', PORT: 8081},
		watch: '*.*',
		ignore: [dist + '/']
	});
});