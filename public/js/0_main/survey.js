$(document).ready(function(){
    // text
    $('.survey--text').on('input', function(e){
        var $survey = $(this).parent().parent();
        $survey.attr("data-answer", $(this).val());
    });

    $(".survey--select").on('change', function(e){
        var $survey = $(this).parent().parent();
        $survey.attr("data-answer", $(this).val());
    });

    // radio
    $(".survey__item-wrapper--radio").click(function(){
        var $survey = $(this).parent();
        var curAnswer = $survey.attr("data-answer");
        var answer = $(this).attr("data-index");
        var isOpen = $survey.attr("data-open");

        if(curAnswer !=="") {
            // 선택된 문항이 있을때
            if(isOpen === "true") {
                //문항이 열려있는 상태인데
                if(curAnswer === answer) {
                    // 현재 선택한 문항이랑 기존 문항이랑 같으면 어떤 변경없이 그냥 닫아주기만한다.
                    $survey.children(".survey__item-wrapper[data-selected='false']").each(function (index, obj) {
                        $(obj).hide(250);
                    });

                    // data-open 스위치
                    $survey.attr("data-open", "false");

                    // indicator 회전
                    $(this).find('.fa').remove();
                    $(this).find('.survey--radio--indicator').append("<span class='fa fa-angle-right radio-rotate-forward'></span>");
                    return;
                } else {
                    // 다른걸 선택했으면 기존꺼 false 처리해주고 신규 아이템 true 처리한다음에 닫아준다.
                    var $postItem = $survey.children(".survey__item-wrapper[data-selected='true']");
                    $postItem.attr('data-selected', "false");
                    $postItem.children().removeClass("selected").addClass("none");
                    $postItem.find('span').removeClass("fa-angle-right").addClass("fa-angle-circle");
                }
            } else {
                // 문항이 닫혀있으면 그냥 열어
                $survey.children(".survey__item-wrapper[data-selected='false']").each(function(index, obj){
                    $(obj).show(250);
                });

                // data-open 스위치
                $survey.attr("data-open", "true");

                // indicator 회전
                //$(this).find('.fa').removeClass('radio-rotate-forward').addClass('radio-rotate-backward');
                $(this).find('.fa').remove();
                $(this).find('.survey--radio--indicator').append("<span class='fa fa-angle-right radio-rotate-backward'></span>");
                return;
            }
        }
        // answer 기입
        $survey.attr("data-answer", answer);

        // data-open 스위치
        $survey.attr("data-open", "false");

        // 선택된 문항 selected 스위치
        $(this).attr("data-selected", "true");
        $(this).children().removeClass("none").addClass("selected");
        $(this).find('.survey--radio--indicator').append("<span class='fa fa-angle-right radio-rotate-forward'></span>");
        $survey.children(".survey__item-wrapper[data-selected='false']").each(function(index, obj){
            $(obj).hide(250);
        });
    });


    // switch
    $(".survey--switch--item").click(function(e) {
        e.preventDefault();
        var $survey = $(this).parent().parent();
        var answer = $(this).attr("data-index");

        // 기존거 false 처리
        $survey.find(".survey--switch--item[data-selected='true']").each(function(index, obj){
            $(obj).attr("data-selected", "false").removeClass("selected");
        });

        // 지금거 true 처리
        $survey.attr("data-answer", answer);
        $(this).attr("data-selected", "true").addClass("selected");
    });

    $(".survey--switch-multi--item").click(function(e) {
        e.preventDefault();
        var $survey = $(this).parent().parent();
        var answer = $(this).attr("data-index");

        // 지금거 true 처리
        var arr = $survey.attr("data-answer");

        if(arr === "") {
            arr = [answer];
        } else {
            arr = JSON.parse(arr);
            arr.push(answer);
        }

        $survey.attr("data-answer", JSON.stringify(arr));
        if($(this).attr("data-selected") === "false") {
            $(this).attr("data-selected", "true").addClass("selected");
        } else {
            $(this).attr("data-selected", "false").removeClass("selected");
        }
    });

});