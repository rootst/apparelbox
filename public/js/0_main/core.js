TweenMax.staggerFrom(".showing", 0.8, {opacity: 0, y: 20, delay: 0.2}, 0.4);

$(document).ready(function() {
    var isJoinLoading = false;

    $('.login-with-facebook').click(function(event) {
        event.preventDefault();
        location.href = '/auth/facebook';
    });

    $('#login-form').submit(function(event) {
        event.preventDefault();

        var email = $('.login-email').val();
        var password = $('.login-password').val();
        var $errorLabel;

        if (!validateEmail(email)) {
            $errorLabel = $('.login-email').next();
            $errorLabel.text('* 올바른 이메일 형식이 아닙니다.');
            $errorLabel.show();
            return;
        }

        var params = {
            email: email,
            password: password,
        };
        $.ajax({
            type: 'POST',
            url: '/login',
            data: params,
            statusCode: {
                401: function(data) {
                    alert('아이디 또는 비밀번호가 다릅니다.');
                },
                404: function(data) {
                    alert('존재하지 않는 계정입니다.');
                }
            },
            success: function(data) {
                window.location.href = data.data;
            }
        });
    });

    $('#join-form').submit(function(event) {
        event.preventDefault();

        if (isJoinLoading) {
            return false;
        }

        var email = $('.join-email').val();
        var displayName = $('.join-displayName').val();
        var password = $('.join-password').val();
        var password2 = $('.join-password2').val();
        var $errorLabel;

        $(this).find('.error').hide();

        if (!validateEmail(email)) {
            $errorLabel = $('.join-email').next();
            $errorLabel.text('* 올바른 이메일 형식이 아닙니다.');
            $errorLabel.show();
            return;
        }

        if (password !== password2) {
            $errorLabel = $('.join-password2').next();
            $errorLabel.text('* 비밀번호가 일치하지 않습니다.');
            $errorLabel.show();
            return;
        }

        var params = {
            email: email,
            password: password,
            displayName: displayName
        };

        $.ajax({
            type: 'POST',
            url: '/api/users',
            data: params,
            beforeSend: function() {
                isJoinLoading = true;
            },
            success: function(data) {
                isJoinLoading = false;
                location.href = '/users/' + data.data._id + '/user_informations/new';
            },
            error: function(data) {
                isJoinLoading = false;
            }
        });
    });

    $(".next-btn").click(function(event) {
        event.preventDefault();

        var next = _.toInteger($(this).attr("data-next"));
    });

    $(".next-btn").click(function(){
        var uiPk = $(".user-information").attr("data-index");
        var next = parseInt($(this).attr("data-next"));
        var params = {};

        if($(".start-btn").attr("data-complete") !== "true") {
            $(".start-btn").click();
            return;
        }

        //버튼 현재 스탭 처리 부분
        $("a.next-step").removeClass("cur");
        $("a[data-step='" + next + "']").addClass("cur");

        $('.step' + (parseInt(next)-1) + ' .survey').each(function (index, obj) {
            var answer = $(this).attr("data-answer");
            if(answer !== "") {
                if($(this).hasClass("survey--switch-multi")) {
                    answer = JSON.parse(answer);
                    //console.log(answer);
                }
                params[$(this).attr("data-field")] = answer;
            }
        });

        $.ajax({
            type: 'PATCH',
            url: '/api/users/' + globalUserId + '/user_informations/' + uiPk,
            contentType: "application/json",
            dataType: 'json',
            data: JSON.stringify(params),
            success: function(res) {

            },
            error: function(res) {

            }
        });

        $(".step" + (next-1).toString()).hide();
        $(".step" + next).show();

        $('html,body').animate({
            scrollTop: $('.user-information').offset().top
        });

    });

    $(".start-btn").click(function(){
        var nickname = $(".survey[data-field='nickname']").attr('data-answer'),
            gender = $(".survey[data-field='gender']").attr('data-answer');

        var uiPk = $(".user-information").attr("data-index");
        var url = '/api/users/' + globalUserId + '/user_informations';
        var type = 'POST';
        var isNew = $(".user-information").attr('data-new');

        if(isNew === "false") {
            type = 'PATCH';
            url = '/api/users/' + globalUserId + '/user_informations/' + uiPk;
        }

        $(this).attr("data-complete", "true");

        $('.step0 > .survey').each(function (index, obj) {
            var answer = $(this).attr("data-answer"),
                field = $(this).attr("data-field");

            if(answer === "") {
                $(".start-btn").attr("data-complete", "false");
                $(this).children(".survey__item-error").show();
                return false;
            }
        });

        //
        if($(this).attr("data-complete") === "true") {
            $(this).attr("data-gender", gender);
            $("a[data-step='0']").addClass("fill");
            $("a.next-step").show();

            $.ajax({
                type: type,
                url: url,
                data: {
                    nickname: nickname,
                    gender: gender
                },
                success: function(res) {
                    $(".user-information").attr("data-index", res.data._id);
                    $(".step0").hide();
                    $(".step1").show();
                },
                error: function(res) {
                    alert('error');
                }
            })

        }
    });

    /*$(".next-step").click(function(event) {
        event.preventDefault();

        var next = _.toInteger(_.replace($(this).attr('href'), '#', ''));
        $(".step" + (next - 1).toString()).hide();
    });*/

    $(".next-step").click(function(e){
        e.preventDefault();


        if($(".start-btn").attr("data-complete") !== "true") {
            $(".start-btn").click();
            return;
        }

        var next = parseInt($(this).attr("href").replace("#", ""));

        //버튼 현재 스탭 처리 부분
        $("a.next-step").removeClass("cur");
        $("a[data-step='" + next + "']").addClass("cur");

        $(".survey-step").hide();
        $(".step" + next).show();
    });

    $(".finish-btn").click(function(event) {
        event.preventDefault();
        var uiPk = $(".user-information").attr("data-index");
        var params = {};
        
        $('.survey').each(function(index, obj) {
            var answer = $(this).attr("data-answer");
            if(answer !== "") {
                if($(this).hasClass("survey--switch-multi")) {
                    answer = JSON.parse(answer);
                    //console.log(answer);
                }
                params[$(this).attr("data-field")] = answer;
            }
            //params[$(this).attr("data-field")] = $(this).attr("data-answer");
        });

        params['status'] = 'COMPLETED';

        $.ajax({
            type: 'PATCH',
            url: '/api/users/' + globalUserId + '/user_informations/' + uiPk,
            contentType: "application/json",
            dataType: 'json',
            data: JSON.stringify(params),
            success: function(res) {
                alert('저장 완료');
                window.location.href='/users/' + globalUserId;
            },
            error: function(res) {

            }
        });
    });

    $('.user-modify-btn').click(function(event) {
        event.preventDefault();

        var params = {
            displayName: $('input[name=user-name]').val(),
            gender: $('select[name=gender]').val(),
            phone: $('input[name=user-phone]').val(),
            status: 'TEMPORARY'
        };
        $.ajax({
            type: 'PATCH',
            url: '/api/users/' + globalUserId,
            data: params,
            statusCode: {
                401: function(data) {
                    //alert('아이디 또는 비밀번호가 다릅니다.');
                },
                404: function(data) {
                    //alert('존재하지 않는 계정입니다.');
                }
            },
            success: function(data) {
                alert('저장 완료');
            }
        });
    });
    
    $(".add-credit-card").click(function(){
        $(".add-card-modal").modal();
    });
    
    $(".add-card-submit").click(function(){
        var cardNumber = $("input[name=card-number]").val(),
            cardCVC = $("input[name=card-cvc]").val(),
            cardYear = $("input[name=card-year]").val(),
            cardMonth = $("input[name=card-month]").val();

        var cardDate = new Date(parseInt(cardYear), parseInt(cardMonth)-1, 1);

        $.ajax({
            type: 'POST',
            url: '/api/users/' + globalUserId + '/cards',
            data: {
                company: "nh",
                number: cardNumber,
                expirationDate: cardDate,
                CVC: cardCVC
            },
            success: function(res) {
                alert("추가완료");
                //console.log(res);
                $(".add-card-modal").modal('hide');
            },
            error: function(res){
                //console.log(res);
            }
        });
    });

    $(".request-submit-btn").click(function(){
        $.ajax({
            type: 'POST',
            url: '/api/users/' + globalUserId + '/user_informations/' + $("select[name='request-info']").val() + '/orders',
            data: {
                type: 'NORMAL',
                address: $("input[name='request-address']").val()
            },
            success: function(res) {
                alert("신청이 완료되었습니다.");
                location.href = "/";
            },
            error: function(res){
                alert("신청 실패");
            }
        });
    });
});