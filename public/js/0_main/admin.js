function loadAdminAuction(filter) {
    var jwtToken = localStorage.getItem('jwt');
    var params = {

    };
    if(filter !== 'all') {
        params.status = filter
    }
    $.ajax({
        type: 'GET',
        url: '/api/auctions',
        headers: {
            'Authorization': 'JWT ' + jwtToken
        },
        data: params,
        success: function (data) {

            var currentPage = parseInt(data.data.page);
            var nextPage = currentPage + 1;

            var $prev = $('.page-prev-a');
            var $next = $('.page-next-a');

            if(currentPage === 1) {
                $prev.hide();
            } else {
                $prev.show();
                $prev.attr('data-page', currentPage-1);
            }

            if(!data.data.hasMore) {
                $next.hide();
            } else {
                $next.show();
                $next.attr('data-page', nextPage);
            }

            if (data.data.total) {
                $.each(data.data.items, function (index, item) {
                    var statusText = "";
                    var statusSummary = "진행중";
                    if (item.status === "PROCESSING") {
                        statusText = "알맞은 공급자들에게 신청이 전달되어 입찰이 진행중입니다.";
                    } else if (item.status === "COMPLETED") {
                        statusText = "공급자 세명의 선정이 완료됐습니다, 공급자 선정 후 결제를 진행해주세요.";
                        statusSummary = "진행중";
                    } else if (item.status === "SUCCESS") {
                        statusText = "결제가 완료되었습니다, 곧 담당자로부터 연락이 갑니다. 조금만 기다려주세요.";
                        statusSummary = "결제완료"
                    } else if (item.status === "FAILURE") {

                    }
                    $('table > tbody').append(`
                      <tr>
                      <td class='col-2'>${timeSince(item.createdDate)}</td>
                      <td class='col-1'>${item.budget}</td>
                      <td class='col-8 status'>
                      ${item.description}
                      </td>
                      <td class='col-1'>${statusSummary}</td>
                      </tr>
                      <tr class='detail-auction'>
                          <td class='col-2'></td>
                          <td class='col-1'></td>
                          <td class='col-8 status'>
                            <div class='auction-detail-wrapper'>
                                <ul>
                                    <li><span class="text-bold"></span></li>
                                    <li><span class="text-bold">인원수</span>: ${item.peopleCounts}</li>
                                    <li><span class="text-bold">선호음식</span>: ${item.cuisines}</li>
                                    <li><span class="text-bold">주소</span>: ${item.serviceAddress}</li>
                                    <li><span class="text-bold">상태</span>: ${item.status}</li>
                                    <li><span class="text-bold">전화번호</span>: 010-0000-0000</li>
                                    <li><span class="text-bold">선택 공급자</span>: ${item.targetSuppliers.length}명</li>
                                    <li class="add-supplier"><input type='text' class='force-bid-userid ${item._id}' placeholder='추가할 유저 아이디'>
                                                    <input type="button" data-index='${item._id}' class="btn btn-primary force-bid-add" value="추가"></li>
                                </ul>
                                <ul class="winner-${item._id}"></ul>
                                <dl class="bid-list-dl ${item._id}">
                                    
                                </dl>
                            </div>
                           </td>
                           <td class='col-1'><input type='button' class='btn btn-primary admin-bid-list-btn' data-index='${item._id}' value='비드목록'></td>
                      </tr>
                    `);
                    if(filter === "-PROCESSING"){
                        $('.add-supplier').remove();
                        if(typeof item.winner !== 'undefined') {
                            $("ul.winner-" + item._id).append(`
                                <li><span class="text-bold">최종 성사된 공급자</span></li>
                                <li>${item.winner.owner.displayName} / 010-0000-0000 / ${item.winner.price}</li>
                            `);
                        }
                    }
                });
            }
        }
    });
}

function toBoolean(auth) {
    return (auth === "true");
}

function loadSuppliers(auth, page) {
    var jwtToken = localStorage.getItem('jwt');
    var params = {};

    if(auth !== 'all') {
        params.authenticated = toBoolean(auth);
    }

    if(page > 1) {
        params.page = page;
    }

    console.log(params);

    $.ajax({
        type: 'GET',
        url: '/api/users',
        headers: {
            'Authorization': 'JWT ' + jwtToken
        },
        data: params,
        success: function(data) {
            var currentPage = parseInt(data.data.page);
            var nextPage = currentPage + 1;
            var $prev;
            var $next;
            if(auth) {
                $prev = $('.suppliersPage-prev-a');
                $next = $('.suppliersPage-next-a');
            } else {
                $prev = $('.suppliersAuthPage-prev-a');
                $next = $('.suppliersAuthPage-next-a');
            }

            if(currentPage === 1) {
                $prev.hide();
            } else {
                $prev.show();
                $prev.attr('data-page', currentPage-1);
            }

            if(!data.data.hasMore) {
                $next.hide();
            } else {
                $next.show();
                $next.attr('data-page', nextPage);
            }

            $.each(data.data.items, function (index, item) {
                if(toBoolean(auth)) {
                    $('table > tbody').append(`
                        <tr>
                            <td>${item.displayName}</td>
                            <td>${item.roles}</td>
                            <td><textarea></textarea></td>
                            <td><input type="button" class="btn btn-primary" value="수정"></td>
                        </tr>
                    `);
                } else {
                    $('table > tbody').append(`
                        <tr class="auth-tr-${item._id}">
                            <td>${item.displayName}</td>
                            <td>${item.phone.number}</td>
                            <td>경력 15년 최고의 요리사</td>
                            <td><input type="button" data-index="${item._id}" class="btn btn-primary supplier-auth-btn" value="인증"></td>
                        </tr>
                    `);
                }
            });
        }
    });
}

$(document).on('click', '.supplier-auth-btn', function() {
    var index = $(this).attr('data-index');
    $.ajax({
        type: "PATCH",
        url: '/api/users/' + index,
        data: {
            authenticated: true,
        },
        statusCode: {
            400: function(data) {

            }
        },
        success: function() {
            alert('해당 공급자에 대한 인증이 처리되었습니다.');
            $('.auth-tr-' + index).remove();
        }
    })
});

$(document).on('click', '.force-bid-add', function(e) {
    var index = $(this).attr('data-index');
    var userId = $('.force-bid-userid.' + index).val();
    var params = {
        targetSuppliers: userId
    };
    $.ajax({
        type: 'PATCH',
        url: '/api/auctions/' + index,
        data: params,
        statusCode: {
            400: function(data) {

            }
        },
        success: function (data) {
            alert('추가완료');
        }
    });
});

$(document).on('click', '.admin-bid-list-btn', function(e){
    var auctionId = $(this).attr('data-index');
    var jwtToken = localStorage.getItem('jwt');
    var params = {
        auction: auctionId
    };
    $(this).remove();
    $.ajax({
        type: 'GET',
        url: '/api/bids',
        headers: {
            'Authorization': 'JWT ' + jwtToken
        },
        data: params,
        success: function(data) {
            if(data.data.total) {
                $('dl.bid-list-dl.' + auctionId).append('<dt>비드 목록(' + data.data.total + ')<dt>');
                $.each(data.data.items, function (index, item) {
                    var serviceDate = new Date(item.auction.serviceDate);
                    $('dl.bid-list-dl.' + auctionId).append(`
                            <dd>공급자: ${item.owner}, 입찰금액: ${item.price}</dd>
                        `)
                });
            }
        }
    });
});

$(document).ready(function() {
    $('.suppliersPage-prev-a').click(function(e){
        e.preventDefault();
        var page = $(this).attr('data-page');
        $('table > tbody').html('');
        loadSuppliers('true', page);
    });

    $('.suppliersPage-next-a').click(function(e){
        e.preventDefault();
        var page = $(this).attr('data-page');
        $('table > tbody').html('');
        loadSuppliers('true', page);
    });

    $('.suppliersAuthPage-prev-a').click(function(e){
        e.preventDefault();
        var page = $(this).attr('data-page');
        $('table > tbody').html('');
        loadSuppliers('false', page);
    });

    $('.suppliersAuthPage-next-a').click(function(e){
        e.preventDefault();
        var page = $(this).attr('data-page');
        $('table > tbody').html('');
        loadSuppliers('false', page);
    });
});