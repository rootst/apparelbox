'use strict';
let Aws = require('aws-sdk');

const config = require('../configs/config');


module.exports = function() {
    Aws.config.region = config.aws.region;
    return new Aws.S3();
};