'use strict';


module.exports = function(app) {
    app.use(function(errorResult, request, response, next) {
        if (!errorResult) {
            return next(); // you also need this line
        }
        console.error(errorResult);
        return response.status(errorResult._status).json({data: response.data});
    });
};