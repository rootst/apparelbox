'use strict';
const config = require('../configs/config');


module.exports = function() {
    return config.env === 'development';
};
