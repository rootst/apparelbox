'use strict';
const _ = require('lodash');

const config = require('../configs/config');


function UserJudge() {
    this._userRoles = _.concat(config.userRole.clients, config.userRole.clerks, config.userRole.administrators);
}

UserJudge.prototype._matchRole = function(roleNames) {
    let ret = false;
    _.forEach(roleNames, (roleName) => {
        if (_.indexOf(this._userRoles, roleName) !== -1) {
            ret = true;
        }
    });
    return ret;
}

UserJudge.prototype.requireRole = function(roleNames) {
    if (!_.isArray(roleNames)) {
        roleNames = [roleNames];
    }

    // role name check
    if (!this._matchRole(roleNames)) {
        return function(request, response, next) {
            let errorMessage = {};
            errorMessage._status = 403;
            next(errorMessage);
        };
    }

    // real check
    let thisObject = this;
    return function(request, response, next) {
        if (thisObject._matchRole(request.user.roles)) {
            next();
        } else {
            let errorMessage = {};
            errorMessage._status = 403;
            next(errorMessage);
        }
    };
};

module.exports = new UserJudge();
