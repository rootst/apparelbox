'use strict';

module.exports = function(request, response, next) {
    if (request.isAuthenticated()) {
        return next();
    }
    let errorMessage = {};
    errorMessage._status = 403;
    next(errorMessage);
};
